use serde::{Deserialize, Serialize};
use tiny_http::{Server, Response};
use rust_bert::{pipelines::conversation::{
    ConversationConfig, ConversationManager, ConversationModel,
}, gpt2::{Gpt2ModelResources, Gpt2VocabResources, Gpt2MergesResources, Gpt2ConfigResources}, resources::RemoteResource, pipelines::common::ModelType};
use std::collections::HashMap;
use uuid::Uuid;

#[derive(Serialize, Deserialize)]
struct MessageRequest {
    number: String,
    message: String
}

#[derive(Serialize, Deserialize)]
struct MessageResponse {
    error: Option<String>,
    message: Option<String>
}

fn main() {
    let server = Server::http("0.0.0.0:8368").unwrap();
    let mut phone_map : HashMap<String, Uuid> = HashMap::new();
    let config = ConversationConfig {
        do_sample: true,
        num_beams: 3,
        temperature: 1.0,
        repetition_penalty: 1.3,
        model_type: ModelType::GPT2,
        model_resource: Box::new(RemoteResource::from_pretrained(
                Gpt2ModelResources::DIALOGPT_SMALL,
        )),
        config_resource: Box::new(RemoteResource::from_pretrained(
                Gpt2ConfigResources::DIALOGPT_SMALL,
        )),
        vocab_resource: Box::new(RemoteResource::from_pretrained(
                Gpt2VocabResources::DIALOGPT_SMALL,
        )),
        merges_resource: Some(Box::new(RemoteResource::from_pretrained(
                    Gpt2MergesResources::DIALOGPT_SMALL,
        ))),
        ..Default::default()
    };
    let conversation_model = ConversationModel::new(config).expect("Loading model");
    let mut conversation_manager = ConversationManager::new();
    for mut request in server.incoming_requests() {
        println!("Received Request");
        if request.method().as_str() != "POST" {
            let response = Response::from_string("{error: 'Failed'}");
            request.respond(response).unwrap_or_else(|_| {println!("Failed to respond")});
            continue
        }
        let text : MessageRequest;
        match serde_json::from_reader(request.as_reader()) {
            Ok(t) => { text = t; },
            Err(_) => {
                println!("Error parsing JSON");
                let response = Response::from_string("{error: 'Failed'}");
                request.respond(response).unwrap_or_else(|_| {println!("Failed to respond")});
                continue
            }
        }
        println!("Responding to {} {}", text.number, text.message);

        let response;
        match phone_map.get(&text.number) {
            Some(id) => {
                let _ = conversation_manager
                    .get(id)
                    .expect("Finding existing conversation")
                    .add_user_input(&text.message);
                conversation_model.generate_responses(&mut conversation_manager);
                response = Some(
                    conversation_manager
                    .get(id)
                    .expect("Finding conversation")
                    .get_last_response()
                    .expect("Getting response")
                    .to_string()
                );
            },
            None => {
                let conversation_id = conversation_manager.create(&text.message);
                phone_map.insert(text.number, conversation_id);
                conversation_model.generate_responses(&mut conversation_manager);
                response = Some(
                    conversation_manager
                    .get(&conversation_id)
                    .expect("Finding conversation")
                    .get_last_response()
                    .expect("Getting response")
                    .to_string()
                );
            }
        }
        match response {
            Some(r) => {
                let res = MessageResponse {
                    message: Some(r),
                        error: None
                };
                let response = Response::from_string(serde_json::to_string(&res).unwrap());
                request.respond(response).expect("Failed to respond");
            },
            None => {
                let response = Response::from_string("{error: 'Failed'");
                request.respond(response).expect("Failed to respond");
            }
        }
    }
        }

