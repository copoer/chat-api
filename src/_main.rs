use std::{
    sync::mpsc,
    thread::{self, JoinHandle},
};
use anyhow::Result;
use actix_web::{web, App, HttpRequest, HttpServer, Responder, web::Data};
use rust_bert::pipelines::conversation::{ConversationModel, ConversationManager};
use std::collections::HashMap;
use uuid::Uuid;
use tokio::{sync::oneshot, task};

async fn greet(req: HttpRequest, data: web::Data<ConversationGenerator>) -> impl Responder {
    let from = req.match_info()
        .get("number").unwrap_or("")
        .to_string();
    let body = req.match_info()
        .get("message").unwrap_or("")
        .to_string();
    println!("Number: {}, Message: {}", from, body);
    let text = SMS {from, body};
    let output = data.predict(text).await.expect("Generating response");
    format!("{}", output)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let (_handle, classifier) = ConversationGenerator::spawn();
    HttpServer::new(move || {
        App::new()
            .app_data(Data::new(classifier.clone()))
            .route("/", web::get().to(greet))
            .route("/{number}/{message}", web::get().to(greet))
    })
    .bind(("127.0.0.1", 8080))?
        .run()
        .await
}

#[derive(Debug, Clone)]
pub struct SMS {
    from: String,
    body: String
}

type Message = (SMS, oneshot::Sender<String>);

#[derive(Debug, Clone)]
pub struct ConversationGenerator {
    sender: mpsc::SyncSender<Message>,
}

impl ConversationGenerator {
    pub fn spawn() -> (JoinHandle<Result<()>>, ConversationGenerator) {
        let (sender, receiver) = mpsc::sync_channel(100);
        let handle = thread::spawn(move || Self::runner(receiver));
        (handle, ConversationGenerator { sender })
    }

    fn runner(receiver: mpsc::Receiver<Message>) -> Result<()> {
        let mut phone_map : HashMap<String, Uuid> = HashMap::new();
        let conversation_model = ConversationModel::new(Default::default())?;
        let mut conversation_manager = ConversationManager::new();
        while let Ok((text, sender))= receiver.recv() {
            let response;
            match phone_map.get(&text.from) {
                Some(id) => {
                    let _ = conversation_manager
                        .get(id)
                        .expect("Finding existing conversation")
                        .add_user_input(&text.body);
                    conversation_model.generate_responses(&mut conversation_manager);
                    response = Some(
                        conversation_manager
                        .get(id)
                        .expect("Finding conversation")
                        .get_last_response()
                        .expect("Getting response")
                        .to_string()
                    );
                },
                None => {
                    let conversation_id = conversation_manager.create(&text.body);
                    phone_map.insert(text.from, conversation_id);
                    conversation_model.generate_responses(&mut conversation_manager);
                    response = Some(
                        conversation_manager
                        .get(&conversation_id)
                        .expect("Finding conversation")
                        .get_last_response()
                        .expect("Getting response")
                        .to_string()
                        );
                }
            }
            sender.send(response.unwrap()).expect("Sending response from chat model");
        }
        Ok(())
    }

    pub async fn predict(&self, text: SMS) -> Result<String> {
        let (sender, receiver) = oneshot::channel();
        task::block_in_place(|| self.sender.send((text, sender)))?;
        Ok(receiver.await?)
    }
    }
